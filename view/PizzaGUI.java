package view;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.RowSorter;
import javax.swing.ScrollPaneConstants;
import static javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS;
import javax.swing.SortOrder;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableRowSorter;
import javax.swing.text.MaskFormatter;
import model.BasketEntity;
import model.Order;
import model.PizzaBase;
import model.PizzaMaterial;
import model.PizzaDough;
import model.PizzaTopping;
import model.User;
import service.PizzaService;
import tablemodel.BaseTableModel;
import tablemodel.BasketTableModel;
import tablemodel.DoughTableModel;
import tablemodel.OrderTableModel;
import tablemodel.ToppingTableModel;
import tablemodel.UserTableModel;

public class PizzaGUI extends JFrame implements ActionListener, MouseListener {

    private static int pizzaSize = 32;
    private JButton btEndOrder, btAddPizza, btDeleteLastPizza;
    private JButton btLogin, btLogout, btWindowExit, btExit, btRegister, btBackToLogin, btRegistration, btLoginWithoutRegistration, btBackToLoginFromAdminPanel, btExitFromAdminPanel;
    private JButton btModifyDough, btModifyBase, btModifyTopping, btModifyUser;
    private JTextField tfDoughPrice, tfBasePrice, tfToppingPrice;
    private JTextField tfUserName, tfUserAddress, tfUserPhone, tfUserPassword;
    private JCheckBox chckBoxUserIsAdmin;
    private JTextField tfLoginName;
    private JPasswordField pfLoginPassword;
    private JFormattedTextField ftfCustomerName;
    private JFormattedTextField ftfCustomerAddress;
    private JTextField ftfCustomerPhoneNumber;
    private JFormattedTextField ftfRegName;
    private JFormattedTextField ftfRegAddress;
    private JFormattedTextField ftfRegPhoneNumber;
    private JPasswordField pfRegPassword;
    private JPasswordField pfRegPasswordCheck;
    private JTextArea taCustomerNote;
    private JTextArea taSummary;
    private JComboBox<PizzaDough> cbTypeOfDough;
    private JComboBox<PizzaBase> cbTypeOfBase;
    private JComboBox<Integer> cbQuantity;
    private JList<PizzaTopping> listTopping;
    private Integer[] quantities = new Integer[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private JRadioButton rbSize32, rbSize45;
    private JCheckBox cbDoubleCheese, cbExtraSauce;
    private boolean isDoubleCheeseChecked = false;
    private boolean isExtraSauceChecked = false;
    private JTable basketTable;
    private BasketTableModel btm;
    private UserTableModel utm;
    private OrderTableModel otm;
    private BaseTableModel basetm;
    private DoughTableModel doughtm;
    private ToppingTableModel toppingtm;
    private ArrayList<PizzaMaterial> onePizzaMaterials;
    private ArrayList<BasketEntity> onePizzaDetails = new ArrayList<>();
    private double endPrice;
    private ArrayList<Order> actualOrders = new ArrayList<>();;
    private User actualUser;
    private BufferedImage[] imgToppings = new BufferedImage[PizzaTopping.toppings.size()];
    private BufferedImage[] imgBases = new BufferedImage[PizzaBase.bases.size()];
    private BufferedImage imgCheese = new BufferedImage(225, 225, 1);
    private BufferedImage imgLogo = new BufferedImage(225, 225, 1);
    private JLabel[] lbl = new JLabel[PizzaTopping.toppings.size() + 2];
    private JLayeredPane layPN = new JLayeredPane();
    private int[] selectedNameIndinces;
    private int rowOfDoughsTable, rowOfBasesTable, rowOfToppingsTable, rowOfUsersTable;
    private CardLayout cardLayout;
    private JPanel mainPanel, loginPanel, registrationPanel, adminPanel, simpleUserPanel;
    private boolean isLoggedUser = false;
    private User loggedUser;
    private int basketCounter;
    private JPanel pnEndButtons;

    public PizzaGUI() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(1500, 700);
        setTitle("Vincenzo Pizzeria");
        try {
            setIconImage(ImageIO.read(new File("src/img/pizza_icon.png")));
        } catch (IOException ex) {
            System.out.println("Nem nyitható meg az icon fájl.");
        }
        setLocationRelativeTo(this);
        setResizable(false);
        Font projectPanelLetter = new Font("Arial", Font.BOLD, 15);
        Font projectTypingLetter = new Font("Courier", Font.PLAIN, 12);

        //Panel váltás cardLayout-tal előkészités. A mainPanel a parent, ő veszi fel a CardLayout-ot.
        cardLayout = new CardLayout();
        mainPanel = new JPanel();
        mainPanel.setLayout(cardLayout);

        //különböző panel-ek létrehozása amelyek cserélődni fognak a mainPanel-en
        loginPanel = new JPanel();
        registrationPanel = new JPanel();
        adminPanel = new JPanel();
        simpleUserPanel = new JPanel();

        //különböző panel-ek hozzáadása a mainPanel-hez.
        mainPanel.add(loginPanel, "loginCard");
        mainPanel.add(registrationPanel, "registrationCard");
        mainPanel.add(simpleUserPanel, "simpleUserCard");
        mainPanel.add(adminPanel, "adminCard");

        
        
        //LOGIN PANEL
        JPanel loginInnerPanel = new JPanel(new BorderLayout());
        JPanel loginNorthPanel = new JPanel();
        JPanel loginCenterPanel = new JPanel();
        JPanel loginSouthPanel = new JPanel();
        loginInnerPanel.add(loginNorthPanel, BorderLayout.NORTH);
        loginInnerPanel.add(loginSouthPanel, BorderLayout.SOUTH);
        loginInnerPanel.add(loginCenterPanel, BorderLayout.CENTER);
        try {
            imgLogo = ImageIO.read(new File("src/img/logovincenzo.png"));
            JLabel logoPic = new JLabel(new ImageIcon(imgLogo));
            loginNorthPanel.add(logoPic);
        } catch (IOException ex) {
            System.out.println("The logo image is missing");;
        }
        JLabel lbLoginName = new JLabel("Név:");
        tfLoginName = new JTextField(10);
        JLabel lbLoginPassword = new JLabel("Jelszó:");
        pfLoginPassword = new JPasswordField(10);
        loginCenterPanel.add(lbLoginName);
        loginCenterPanel.add(tfLoginName);
        loginCenterPanel.add(lbLoginPassword);
        loginCenterPanel.add(pfLoginPassword);

        btLogin = new JButton("BEJELENTKEZÉS");
        btLogin.addActionListener(this);
        btWindowExit = new JButton("KILÉPÉS");
        btWindowExit.addActionListener(this);
        btRegister = new JButton("REGISZTRÁCIÓ");
        btRegister.addActionListener(this);
        btLoginWithoutRegistration = new JButton("RENDELÉS REGISZTRÁCIÓ NÉLKÜL");
        btLoginWithoutRegistration.addActionListener(this);
        loginSouthPanel.add(btLogin);
        loginSouthPanel.add(btWindowExit);
        loginSouthPanel.add(btRegister);
        loginSouthPanel.add(btLoginWithoutRegistration);
        loginPanel.add(loginInnerPanel);

        
        
        //REGISTRATION PANEL
        JPanel pnRegFirstCol = new JPanel();
        JPanel pnRegCol = new JPanel(new GridLayout(2, 1));
        JPanel pnRegThirdCol = new JPanel();
        JPanel pnRegColFirst = new JPanel(new GridLayout(5, 2, 0, 15));
        JPanel pnRegColSecond = new JPanel();
        
        TitledBorder bdRegistration = new TitledBorder("FELHASZNÁLÓI REGISZTRÁCIÓ");
        bdRegistration.setTitleJustification(TitledBorder.CENTER);
        pnRegColFirst.setBorder(bdRegistration);

        JLabel lbRegName = new JLabel("Név:", JLabel.CENTER);
        JLabel lbRegAddress = new JLabel("Cím:", JLabel.CENTER);
        JLabel lbRegPhoneNumber = new JLabel("Telefonszám:", JLabel.CENTER);
        JLabel lbRegPassword = new JLabel("Jelszó:", JLabel.CENTER);
        JLabel lbRegPasswordCheck = new JLabel("Jelszó újra:", JLabel.CENTER);
        ftfRegName = new JFormattedTextField();
        ftfRegAddress = new JFormattedTextField();
        ftfRegPhoneNumber = new JFormattedTextField();
        pfRegPassword = new JPasswordField();
        pfRegPasswordCheck = new JPasswordField();

        btRegistration = new JButton("REGISZTRÁCIÓ");
        btRegistration.addActionListener(this);
        btBackToLogin = new JButton("VISSZA A BEJELENTKEZÉSHEZ");
        btBackToLogin.addActionListener(this);

        pnRegCol.add(pnRegColFirst);
        pnRegCol.add(pnRegColSecond);
        pnRegColFirst.add(lbRegName);
        pnRegColFirst.add(ftfRegName);
        pnRegColFirst.add(lbRegAddress);
        pnRegColFirst.add(ftfRegAddress);
        pnRegColFirst.add(lbRegPhoneNumber);
        try {
            MaskFormatter mfRegPhoneNumber = new MaskFormatter("## ## ### ####");
            mfRegPhoneNumber.setPlaceholderCharacter(' ');
            ftfRegPhoneNumber = new JFormattedTextField(mfRegPhoneNumber);
            pnRegColFirst.add(ftfRegPhoneNumber);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        pnRegColFirst.add(lbRegPassword);
        pnRegColFirst.add(pfRegPassword);
        pnRegColFirst.add(lbRegPasswordCheck);
        pnRegColFirst.add(pfRegPasswordCheck);
        pnRegColSecond.add(btRegistration);
        pnRegColSecond.add(btBackToLogin);
        registrationPanel.add(pnRegFirstCol);
        registrationPanel.add(pnRegCol);
        registrationPanel.add(pnRegThirdCol);
        
        
        
        
        //ADMIN PANEL - DOUGH TAB
        JTextField tfDoughName = new JTextField("Név", 10);
        tfDoughName.setEnabled(false);
        tfDoughPrice = new JTextField("Ár", 10);
        btModifyDough = new JButton("Módositás");
        btModifyDough.addActionListener(this);
        JPanel doughsControlPanel = new JPanel();
        doughsControlPanel.add(tfDoughName);
        doughsControlPanel.add(tfDoughPrice);
        doughsControlPanel.add(btModifyDough);
        List<PizzaDough> doughMaterials = new ArrayList<>(PizzaDough.getPizzaDoughs());
        doughtm = new DoughTableModel(doughMaterials);
        JTable doughsTable = new JTable(doughtm);
        doughsTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                rowOfDoughsTable = table.rowAtPoint(p);
                if (me.getClickCount() == 1) {
                    System.out.println("Ez a sor lett kiválasztva: "+rowOfDoughsTable);
                    tfDoughName.setText(doughtm.getValueAt(rowOfDoughsTable, 0).toString());
                    tfDoughPrice.setText(doughtm.getValueAt(rowOfDoughsTable, 1).toString());
                }
            }
        });
        JScrollPane doughsTableScrollPane = new JScrollPane(doughsTable);
        JPanel doughsPanel = new JPanel();
        doughsPanel.setLayout(new BorderLayout());
        doughsPanel.add(doughsControlPanel, BorderLayout.NORTH);
        doughsPanel.add(doughsTableScrollPane, BorderLayout.CENTER);
        
        //ADMIN PANEL - BASE TAB
        JTextField tfBaseName = new JTextField("Név", 10);
        tfBaseName.setEnabled(false);
        tfBasePrice = new JTextField("Ár", 10);
        btModifyBase = new JButton("Módositás");
        btModifyBase.addActionListener(this);
        JPanel basesControlPanel = new JPanel();
        basesControlPanel.add(tfBaseName);
        basesControlPanel.add(tfBasePrice);
        basesControlPanel.add(btModifyBase);
        List<PizzaBase> baseMaterials = new ArrayList<>(PizzaBase.getPizzaBases());
        basetm = new BaseTableModel(baseMaterials);
        JTable basesTable = new JTable(basetm);
        basesTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                rowOfBasesTable = table.rowAtPoint(p);
                if (me.getClickCount() == 1) {
                    tfBaseName.setText(basetm.getValueAt(rowOfBasesTable, 0).toString());
                    tfBasePrice.setText(basetm.getValueAt(rowOfBasesTable, 1).toString());
                }
            }
        });
        JScrollPane basesTableScrollPane = new JScrollPane(basesTable);
        JPanel basesPanel = new JPanel();
        basesPanel.setLayout(new BorderLayout());
        basesPanel.add(basesControlPanel, BorderLayout.NORTH);
        basesPanel.add(basesTableScrollPane, BorderLayout.CENTER);
        
        //ADMIN PANEL - TOPPING TAB
        JTextField tfToppingName = new JTextField("Név", 10);
        tfToppingName.setEnabled(false);
        tfToppingPrice = new JTextField("Ár", 10);
        btModifyTopping = new JButton("Módositás");
        btModifyTopping.addActionListener(this);
        JPanel toppingsControlPanel = new JPanel();
        toppingsControlPanel.add(tfToppingName);
        toppingsControlPanel.add(tfToppingPrice);
        toppingsControlPanel.add(btModifyTopping);
        List<PizzaTopping> toppingMaterials = new ArrayList<>(PizzaTopping.getPizzaToppings());
        toppingtm = new ToppingTableModel(toppingMaterials);
        JTable toppingsTable = new JTable(toppingtm);
        toppingsTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                rowOfToppingsTable = table.rowAtPoint(p);
                if (me.getClickCount() == 1) {
                    tfToppingName.setText(toppingtm.getValueAt(rowOfToppingsTable, 0).toString());
                    tfToppingPrice.setText(toppingtm.getValueAt(rowOfToppingsTable, 1).toString());
                }
            }
        });
        JScrollPane toppingsTableScrollPane = new JScrollPane(toppingsTable);
        JPanel toppingsPanel = new JPanel();
        toppingsPanel.setLayout(new BorderLayout());
        toppingsPanel.add(toppingsControlPanel, BorderLayout.NORTH);
        toppingsPanel.add(toppingsTableScrollPane, BorderLayout.CENTER);

        //ADMIN PANEL - USER TAB
        tfUserName = new JTextField("Név", 10);
        tfUserName.setEnabled(false);
        tfUserPassword = new JTextField("Jelszó", 10);
        tfUserAddress = new JTextField("Cim", 10);
        tfUserPhone = new JTextField("Telefon", 10);
        chckBoxUserIsAdmin = new JCheckBox();
        chckBoxUserIsAdmin.setSelected(false);
        btModifyUser = new JButton("Módositás");
        btModifyUser.addActionListener(this);
        JPanel usersControlPanel = new JPanel();
        usersControlPanel.add(tfUserName);
        usersControlPanel.add(tfUserPassword);
        usersControlPanel.add(tfUserAddress);
        usersControlPanel.add(tfUserPhone);
        usersControlPanel.add(chckBoxUserIsAdmin);
        usersControlPanel.add(btModifyUser);
        List<User> users = new ArrayList<>(User.getUsers());
        utm = new UserTableModel(users);
        JTable usersTable = new JTable(utm);
        usersTable.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                JTable table = (JTable) me.getSource();
                Point p = me.getPoint();
                rowOfUsersTable = table.rowAtPoint(p);
                if (me.getClickCount() == 1) {
                    tfUserName.setText(utm.getValueAt(rowOfUsersTable, 0).toString());
                    tfUserPassword.setText(utm.getValueAt(rowOfUsersTable, 1).toString());
                    tfUserAddress.setText(utm.getValueAt(rowOfUsersTable, 2).toString());
                    tfUserPhone.setText(utm.getValueAt(rowOfUsersTable, 3).toString());
                    chckBoxUserIsAdmin.setSelected((boolean) utm.getValueAt(rowOfUsersTable, 4));
                }
            }
        });
        JScrollPane usersTableScrollPane = new JScrollPane(usersTable);
        JPanel usersPanel = new JPanel();
        usersPanel.setLayout(new BorderLayout());
        usersPanel.add(usersControlPanel, BorderLayout.NORTH);
        usersPanel.add(usersTableScrollPane, BorderLayout.CENTER);

        //ADMIN PANEL - ORDER TAB
        List<Order> orders = new ArrayList<>(Order.getOrders());
        otm = new OrderTableModel(orders);
        JTable ordersTable = new JTable(otm);
        //Cella szélességek beálllitása.
        ordersTable.getColumnModel().getColumn(2).setPreferredWidth(150);
        ordersTable.getColumnModel().getColumn(4).setPreferredWidth(150);
        ordersTable.getColumnModel().getColumn(11).setPreferredWidth(150);
        ordersTable.getColumnModel().getColumn(12).setPreferredWidth(100);
        ordersTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        //Rendezzük az Orders táblát orderId szerint, hogy mindig a legfrisebb rendelés legyen felül.
        TableRowSorter<OrderTableModel> sorter = new TableRowSorter<OrderTableModel>(otm);
        ordersTable.setRowSorter(sorter);
        List<RowSorter.SortKey> sortKeys = new ArrayList<>(otm.getColumnCount());
        sortKeys.add(new RowSorter.SortKey(0, SortOrder.DESCENDING));
        sorter.setSortKeys(sortKeys);
        //Rátesszük a táblát egy scrollPane-re, hogy vertikálisan görgethető legyen
        JScrollPane ordersTableScrollPane = new JScrollPane(ordersTable);
        ordersTableScrollPane.setVerticalScrollBarPolicy(VERTICAL_SCROLLBAR_ALWAYS);
        JPanel ordersPanel = new JPanel();
        ordersPanel.setLayout(new BorderLayout());
        ordersPanel.add(ordersTableScrollPane, BorderLayout.CENTER);

        //ADMIN PANEL - TAB-OK, VISSZA ÉS KILÉPÉS GOMBOK
        JTabbedPane tabbedPane = new JTabbedPane();
        tabbedPane.addTab("Doughs", doughsPanel);
        tabbedPane.addTab("Bases", basesPanel);
        tabbedPane.addTab("Toppings", toppingsPanel);
        tabbedPane.addTab("Users", usersPanel);
        tabbedPane.addTab("Orders", ordersPanel);

        JPanel adminControlPanel = new JPanel();
        btBackToLoginFromAdminPanel = new JButton("Vissza a bejelentkezéshez");
        btBackToLoginFromAdminPanel.addActionListener(this);
        btExitFromAdminPanel = new JButton("Kilépés");
        btExitFromAdminPanel.addActionListener(this);
        adminControlPanel.add(btBackToLoginFromAdminPanel);
        adminControlPanel.add(btExitFromAdminPanel);

        adminPanel.setLayout(new BorderLayout());
        adminPanel.add(tabbedPane, BorderLayout.CENTER);
        adminPanel.add(adminControlPanel, BorderLayout.SOUTH);

        
        
        
        //SIMPLE USER PANEL
        simpleUserPanel.setLayout(new GridLayout(2, 1));
        JPanel pnFirstRow = new JPanel(new GridLayout(1, 4));
        JPanel pnSecondRow = new JPanel(new GridLayout(1, 2));
        JPanel pnDataContainer = new JPanel(new GridLayout(3, 1));
        JPanel pnCustomerData = new JPanel(new GridLayout(3, 2));
        JPanel pnPizzaMixerFirst = new JPanel(new GridLayout(4, 2));
        JPanel pnPizzaMixerSecond = new JPanel(new GridLayout(4, 2));
        JPanel pnBasketTable = new JPanel();
        JPanel pnPizzaImage = new JPanel();
        JPanel pnSumOrder = new JPanel(new GridLayout(1, 2));
        JPanel pnSumOrderFirst = new JPanel();
        JPanel pnSumOrderSecond = new JPanel(new GridLayout(2, 1));
        JPanel pnEmpty = new JPanel();
        pnEndButtons = new JPanel();

        //BORDEREK ILLESZTÉSE
        TitledBorder bdCustomerData = new TitledBorder("SZEMÉLYES ADATOK");
        bdCustomerData.setTitleJustification(TitledBorder.CENTER);
        TitledBorder bdPizzaMixer = new TitledBorder("PIZZA ÖSSZEÁLLÍTÁSA");
        bdPizzaMixer.setTitleJustification(TitledBorder.CENTER);
        TitledBorder bdPizzaImage = new TitledBorder("PIZZA KÉP");
        bdPizzaImage.setTitleJustification(TitledBorder.CENTER);
        TitledBorder bdJTable = new TitledBorder("HOZZÁADOTT PIZZÁK");
        bdJTable.setTitleJustification(TitledBorder.CENTER);
        pnBasketTable.setBorder(bdJTable);
        pnPizzaImage.setBorder(bdPizzaImage);
        pnCustomerData.setBorder(bdCustomerData);
        pnPizzaMixerFirst.setBorder(bdPizzaMixer);
        pnPizzaMixerSecond.setBorder(bdPizzaMixer);

        simpleUserPanel.add(pnFirstRow);
        simpleUserPanel.add(pnSecondRow);

        pnFirstRow.add(pnDataContainer);
        pnDataContainer.add(pnCustomerData);
        pnFirstRow.add(pnPizzaMixerFirst);
        pnFirstRow.add(pnPizzaMixerSecond);
        pnFirstRow.add(layPN);
        pnSecondRow.add(pnBasketTable);
        pnSecondRow.add(pnSumOrder);
        pnSumOrder.add(pnSumOrderFirst);
        pnSumOrder.add(pnSumOrderSecond);
        pnSumOrderSecond.add(pnEmpty);
        pnSumOrderSecond.add(pnEndButtons);

        //CUSTOMER ADATOK MEGADÁSA
        JLabel lbCustomerName = new JLabel("Név:*", JLabel.CENTER);
        lbCustomerName.setFont(projectPanelLetter);
        lbCustomerName.setForeground(Color.BLACK);
        pnCustomerData.add(lbCustomerName);
        try {
            MaskFormatter mfCustomerName = new MaskFormatter("U****************");
            mfCustomerName.setInvalidCharacters("#'");
            ftfCustomerName = new JFormattedTextField(mfCustomerName);
            ftfCustomerName.setFont(projectTypingLetter);
            pnCustomerData.add(ftfCustomerName);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JLabel lbCustomerAddress = new JLabel("Cim:*", JLabel.CENTER);
        lbCustomerAddress.setFont(projectPanelLetter);
        lbCustomerAddress.setForeground(Color.BLACK);
        pnCustomerData.add(lbCustomerAddress);
        ftfCustomerAddress = new JFormattedTextField();
        ftfCustomerAddress.setFont(projectTypingLetter);
        pnCustomerData.add(ftfCustomerAddress);

        JLabel lbCustomerPhoneNumber = new JLabel("Telefonszám:*", JLabel.CENTER);
        lbCustomerPhoneNumber.setFont(projectPanelLetter);
        lbCustomerPhoneNumber.setForeground(Color.BLACK);
        pnCustomerData.add(lbCustomerPhoneNumber);
        try {
            MaskFormatter mfCustomerPhoneNumber = new MaskFormatter("## ## ### ####");
            mfCustomerPhoneNumber.setPlaceholderCharacter(' ');
            ftfCustomerPhoneNumber = new JFormattedTextField(mfCustomerPhoneNumber);
            ftfCustomerPhoneNumber.setFont(projectTypingLetter);
            pnCustomerData.add(ftfCustomerPhoneNumber);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //KÉP FÁJLOK BETÖLTÉSE
        try {
            imgCheese = ImageIO.read(new File("src/img/cheese.png"));
            int arrayIndex = 0;
            for (String base : PizzaBase.bases.keySet()) {
                imgBases[arrayIndex] = ImageIO.read(new File("src/img/" + base + ".png"));
                arrayIndex++;
            }
            arrayIndex = 0;
            for (String topping : PizzaTopping.toppings.keySet()) {
                imgToppings[arrayIndex] = ImageIO.read(new File("src/img/" + topping + ".png"));
                arrayIndex++;
            }
        } catch (IOException ex) {
            System.out.println("The images are missing");;
        }

        //PIZZA ÖSSZEÁLLÍTÁS: 2DB COMBOBOX, 1DB JLIST
        JLabel lbPizzaDough = new JLabel("Pizzatészták:", JLabel.CENTER);
        lbPizzaDough.setFont(projectPanelLetter);
        lbPizzaDough.setForeground(Color.BLACK);
        cbTypeOfDough = new JComboBox<>();
        //cbTypeOfDough.setModel(new DefaultComboBoxModel(PizzaDough.doughs.keySet().toArray()));
        cbTypeOfDough.setModel(new DefaultComboBoxModel<PizzaDough>(PizzaDough.getPizzaDoughs().toArray(new PizzaDough[]{null})));
        pnPizzaMixerFirst.add(lbPizzaDough);
        pnPizzaMixerFirst.add(cbTypeOfDough);
        
        JLabel lbPizzaBase = new JLabel("Pizza alap:", JLabel.CENTER);
        lbPizzaBase.setFont(projectPanelLetter);
        lbPizzaBase.setForeground(Color.BLACK);
        cbTypeOfBase = new JComboBox<>();
        //cbTypeOfBase.setModel(new DefaultComboBoxModel(PizzaBase.bases.keySet().toArray()));
        cbTypeOfBase.setModel(new DefaultComboBoxModel<PizzaBase>(PizzaBase.getPizzaBases().toArray(new PizzaBase[]{null})));
        cbTypeOfBase.addActionListener(this);
        pnPizzaMixerFirst.add(lbPizzaBase);
        pnPizzaMixerFirst.add(cbTypeOfBase);
        
        JLabel lbPizzaToppings = new JLabel("Feltétek:", JLabel.CENTER);
        lbPizzaToppings.setFont(projectPanelLetter);
        lbPizzaToppings.setForeground(Color.BLACK);
        listTopping = new JList(PizzaTopping.getPizzaToppings().toArray(new PizzaTopping[]{null}));
        //listTopping.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        listTopping.setSelectionModel(new DefaultListSelectionModel() {
            @Override
            public void setSelectionInterval(int index0, int index1) {
                if (super.isSelectedIndex(index0)) {
                    super.removeSelectionInterval(index0, index1);
                } else {
                    super.addSelectionInterval(index0, index1);
                }
                fireValueChanged(index0, index1);
            }
        });
        JScrollPane scrPaneTopping = new JScrollPane(listTopping);
        listTopping.addMouseListener(this);
        pnPizzaMixerFirst.add(lbPizzaToppings);
        pnPizzaMixerFirst.add(scrPaneTopping);
        
        //RADIO GOMBOK - PIZZA MÉRET
        rbSize32 = new JRadioButton("32 cm");
        rbSize32.setHorizontalAlignment(SwingConstants.CENTER);
        //rbSize32.setMnemonic(KeyEvent.VK_B);
        rbSize32.setActionCommand("32 cm");
        rbSize32.addActionListener(this);
        rbSize32.setSelected(true);
        rbSize45 = new JRadioButton("45 cm");
        rbSize45.setHorizontalAlignment(SwingConstants.CENTER);
        //rbSize45.setMnemonic(KeyEvent.VK_B);
        rbSize45.setActionCommand("45 cm");
        rbSize45.addActionListener(this);
        //Össszecsoportositjuk a rádió gombokat
        ButtonGroup group = new ButtonGroup();
        group.add(rbSize32);
        group.add(rbSize45);
        pnPizzaMixerFirst.add(rbSize32);
        pnPizzaMixerFirst.add(rbSize45);
        
        //CHECKBOXOK - DUPLA SAJT, EXTRA SZÓSZ
        cbDoubleCheese = new JCheckBox("dupla sajt");
        cbDoubleCheese.setHorizontalAlignment(SwingConstants.CENTER);
        cbDoubleCheese.addActionListener(this);
        pnPizzaMixerSecond.add(cbDoubleCheese);
        cbExtraSauce = new JCheckBox("extra szósz");
        cbExtraSauce.setHorizontalAlignment(SwingConstants.CENTER);
        cbExtraSauce.addActionListener(this);
        pnPizzaMixerSecond.add(cbExtraSauce);
        
        //DARABSZÁM
        JLabel lbQuantity = new JLabel("Darabszám:", JLabel.CENTER);
        lbQuantity.setFont(projectPanelLetter);
        lbQuantity.setForeground(Color.BLACK);
        cbQuantity = new JComboBox(quantities);
        pnPizzaMixerSecond.add(lbQuantity);
        pnPizzaMixerSecond.add(cbQuantity);
        
        //ORDER MEGJEGYZÉS
        JLabel lbCustomerNote = new JLabel("Megjegyzés:", JLabel.CENTER);
        lbCustomerNote.setFont(projectPanelLetter);
        lbCustomerNote.setForeground(Color.BLACK);
        taCustomerNote = new JTextArea(10, 20);
        JScrollPane scrPaneNote = new JScrollPane(taCustomerNote);
        scrPaneNote.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        pnPizzaMixerSecond.add(lbCustomerNote);
        pnPizzaMixerSecond.add(scrPaneNote);
        
        //BASKET TABLE ÉS UTOLSÓ TÖRLÉSE GOMB KÉSZITÉSE
        btm = new BasketTableModel(onePizzaDetails);
        basketTable = new JTable(btm);
        basketTable.getColumnModel().getColumn(0).setPreferredWidth(20);
        basketTable.getColumnModel().getColumn(1).setPreferredWidth(5);
        basketTable.getColumnModel().getColumn(2).setPreferredWidth(30);
        basketTable.getColumnModel().getColumn(3).setPreferredWidth(30);
        basketTable.getColumnModel().getColumn(4).setPreferredWidth(150);
        basketTable.getColumnModel().getColumn(5).setPreferredWidth(100);
        basketTable.getColumnModel().getColumn(6).setPreferredWidth(5);
        basketTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
        JScrollPane scrPaneBasketTable = new JScrollPane(basketTable);
        scrPaneBasketTable.setPreferredSize(new Dimension(700, 250));
        pnBasketTable.add(scrPaneBasketTable);
        btDeleteLastPizza = new JButton("UTOLSÓ TÖRLÉSE");
        btDeleteLastPizza.addActionListener(this);
        pnBasketTable.add(btDeleteLastPizza);
        
        //PIZZA HOZZÁADÁS GOMB
        btAddPizza = new JButton("PIZZA HOZZÁADÁSA");
        pnPizzaMixerSecond.add(btAddPizza);
        btAddPizza.addActionListener(this);
        
        //ÖSSZESITÉS TEXTAREA
        JLabel lbSummaryOfOrder = new JLabel("Összesités:", JLabel.RIGHT);
        lbSummaryOfOrder.setFont(projectPanelLetter);
        lbSummaryOfOrder.setForeground(Color.BLACK);
        pnSumOrderFirst.add(lbSummaryOfOrder);
        taSummary = new JTextArea(17, 30);
        taSummary.setEditable(false);
        JScrollPane scrPaneSummary = new JScrollPane(taSummary);
        scrPaneSummary.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        pnSumOrderFirst.add(scrPaneSummary);
        
        //RENDELÉS VÉGLEGESITÉSE ÉS KILÉPÉS GOMB
        btEndOrder = new JButton("RENDELÉS VÉGLEGESÍTÉSE");
        pnEndButtons.add(btEndOrder);
        btEndOrder.addActionListener(this);
        btExit = new JButton("KILÉPÉS");
        pnEndButtons.add(btExit);
        btExit.addActionListener(this);

        //MAIN PANEL HOZZÁADÁSA A FRAME-HEZ ÉS LÁTHATÓVÁ TÉTELE
        add(mainPanel);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btLogin) {
            List<User> users = new ArrayList<>(User.getUsers());
            boolean wrongCredentials = true;
            String userName = tfLoginName.getText().trim();
            String userPassword = String.valueOf(pfLoginPassword.getPassword());
            int i = 0;
            while (wrongCredentials == true && i < users.size()) {
                wrongCredentials = true;
                //Ellenűrizzük, hogy a név és a jelszó megtalálható-e a User HashMap-ben, és hogy a jelszó mező nem üres-e.
                if ((userName.equals(users.get(i).getName()) && (userPassword.equals(users.get(i).getPassword()))) && (!userPassword.trim().isEmpty())) {
                    wrongCredentials = false;
                    if (users.get(i).isIsAdmin()) {
                        System.out.println("Váltás az admin panelra.");
                        cardLayout.show(mainPanel, "adminCard");
                    } else {
                        System.out.println("Váltás a regisztált pizza rendelő panelra.");
                        ftfCustomerName.setText(users.get(i).getName());
                        ftfCustomerAddress.setText(users.get(i).getAddress());
                        ftfCustomerPhoneNumber.setText(users.get(i).getPhone());
                        //Bejelentkezéskor töröljük a pizza összeállitások listát, hogy az összegzésben (taSmmary) ne lássa a user, hogy mit rendelt az elöző felhasználó.
                        actualOrders.clear();
                        btExit.setText("KIJELENTKEZÉS");
                        cardLayout.show(mainPanel, "simpleUserCard");
                    }
                }
                i++;
            }
            if (wrongCredentials) {
                JOptionPane.showMessageDialog(null, "A felhasználónév vagy a jelszó nem egyezik.");
                tfLoginName.setText("");
                pfLoginPassword.setText("");
            }
        }
        if (e.getSource() == btBackToLoginFromAdminPanel) {
            cardLayout.show(mainPanel, "loginCard");
            tfLoginName.setText("");
            pfLoginPassword.setText("");
        }
        if (e.getSource() == btExit) {
            if(btExit.getText().equals("KIJELENTKEZÉS")){
                cardLayout.show(mainPanel, "loginCard");
                tfLoginName.setText("");
                pfLoginPassword.setText("");
                taSummary.setText("");
                cbDoubleCheese.setSelected(false);
                cbExtraSauce.setSelected(false);
                cbQuantity.setSelectedIndex(0);
                rbSize32.setSelected(true);
                taCustomerNote.setText("");
                //selectedToppings.clear();
                listTopping.clearSelection();
                btm.clearBasket();
                layPN.removeAll();
                System.out.println("Váltás a login panelra.");
            }else{
                System.exit(0);
            }    
        }
        if (e.getSource() == btExitFromAdminPanel|| e.getSource() == btWindowExit) {
           System.exit(0);
        }
        if (e.getSource() == btLogout) {
            cardLayout.show(mainPanel, "LoginCard");
            System.out.println("Váltás a login panelra.");
        }
        if (e.getSource() == btRegister) {
            cardLayout.show(mainPanel, "registrationCard");
            System.out.println("Váltás a regisztációs panelra.");
        }
        if (e.getSource() == btLoginWithoutRegistration) {
            layPN.removeAll();
            ftfCustomerName.setText("");
            ftfCustomerAddress.setText("");
            ftfCustomerPhoneNumber.setText("");
            btExit.setText("KILÉPÉS");
            cardLayout.show(mainPanel, "simpleUserCard");
            System.out.println("Váltás a pizza rendelő panelra.");
        }
        //ITT KERÜL BELE A BASKET TÁBLÁBA EGY PIZZA ÖSSZEÁLLITÁS, ÉS ITT HOZUNK LÉTRE EGY ORDER PÉLDÁNYT.
        if (e.getSource() == btAddPizza) {
            if (!ftfCustomerName.getText().equals("") && !ftfCustomerAddress.getText().equals("") && !ftfCustomerPhoneNumber.getText().equals("")) {
                if (listTopping.getSelectedIndex() == -1) {
                    JOptionPane.showMessageDialog(null, "Kérem válasszon ki legalább egy feltétet!!");
                } else {
                    //Vizsgálni kell, hogy bejelentkezett, vagy nem regisztrált felhasználó rendelésről van szó.
                    for (User user : User.getUsers()) {
                        if(user.getName().equals(ftfCustomerName.getText().trim())){
                            isLoggedUser = true;
                        }
                    }
                    if(!isLoggedUser){
                        actualUser = new User(ftfCustomerName.getText().trim(), "", ftfCustomerAddress.getText().trim(), ftfCustomerPhoneNumber.getText().trim(), false);
                    }else{
                        actualUser = new User(ftfCustomerName.getText().trim(), User.getUser(ftfCustomerName.getText().trim()).getPassword(), ftfCustomerAddress.getText().trim(), ftfCustomerPhoneNumber.getText().trim(), User.getUser(ftfCustomerName.getText().trim()).isIsAdmin());
                    }
                    //A kiválasztott tésztát és alapot elmentjük egy-egy objektumba.
                    PizzaDough selectedPizzaDough = PizzaDough.getPizzaDough(cbTypeOfDough.getSelectedItem().toString());
                    PizzaBase selectedPizzaBase = PizzaBase.getPizzaBase(cbTypeOfBase.getSelectedItem().toString());
                    //A kiválasztott feltéteket elmentjük egy feltét objektumokat tartalmazó listába.
                    ArrayList<PizzaTopping> selectedToppings  = new ArrayList<>(listTopping.getSelectedValuesList());
                    //Létrehozunk egy olyan listát amelyben egy rendelés  tésztáját, alapját és a feltéteit is tárolni tudjuk.
                    onePizzaMaterials = new ArrayList<>();
                    //Minden kiválasztást hozzáadunk a listánkhoz. Fontos a sorrend. 0=tészta, 1=alap, 2=topping1.....
                    onePizzaMaterials.add(selectedPizzaDough);
                    onePizzaMaterials.add(selectedPizzaBase);
                    onePizzaMaterials.addAll(selectedToppings);
                    //Meghivjuk a PizzaService osztály egy példányán a calculateOnePizzaPrice() függvényt átadva neki a pizza alapanyagokat tároló listánkat.
                    double price = new PizzaService().calculateOnePizzaPrice(onePizzaMaterials, (Integer) cbQuantity.getSelectedItem(), isExtraSauceChecked, isDoubleCheeseChecked);
                    //A checkBox kiválasztásokat még hozzáadjuk a BasketTableModel-hez, ami feltölti a basket JTable-t.
                    String[] extras = new String[2];
                    if (isExtraSauceChecked) {
                        extras[0] =("extra " + cbTypeOfBase.getSelectedItem().toString());
                    }
                    if (isDoubleCheeseChecked) {
                        extras[1] = "dupla sajt";
                    }
                    //Lekérdezzük a pizza méret kiválasztás rádió gombot.
                    int size = (rbSize32.isSelected()? 32 : 45);
                    //A Basket Table Modelhez hozzáadunk egy pizza kiválasztást.
                    btm.addRow(new BasketEntity((Integer)cbQuantity.getSelectedItem(), size, selectedPizzaDough, selectedPizzaBase, selectedToppings, extras, price));
                    endPrice += price;
                    //Az OrderId-t kiszámoljuk. 1000 + Eddigi order-ek száma + Basket számláló, amit növelünk ahányszor az aktuális felhasználó pizzát állit össze.
                    int actualOrderID = 0;
                    basketCounter++;
                    actualOrderID = Order.getInitOrderId() + Order.orders.size() + basketCounter;
                    //Elkészitünk egy rendelést az adatokkal.
                    Order userOrder = null;
                    userOrder = new Order(actualOrderID, actualUser, LocalDate.now(), taCustomerNote.getText(), price, (Integer) cbQuantity.getSelectedItem(), size, selectedPizzaDough, selectedPizzaBase, selectedToppings, extras);
                    //Az aktuális rendelés ArrayList-be beletesszük a rendelést.
                    actualOrders.add(new Order(actualOrderID, actualUser, LocalDate.now(), taCustomerNote.getText(), price, (Integer) cbQuantity.getSelectedItem(), size, selectedPizzaDough, selectedPizzaBase, selectedToppings, extras));
                    //Az aktuális rendelést hozzáadjuk a OrderTableModel-hez, hogy megjelenitse az Admin felületen az Orders táblában.
                    otm.addRow(userOrder);
                    //Kitöröljük a felhasználói megjegyzést, hogy új pizza összeállitshoz újjat tudjon irni, és ne kelljen neki törölnie.
                    taCustomerNote.setText("");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Kérem töltse ki a név, cim, telefon mezőket!");
            }
        }
        if (e.getSource() == btDeleteLastPizza) {
            if(btm.getRowCount()!=0){
                //Az OrderTableModel-ből és a BasketTableModel-ből is törljük az utolsó sort, hogy ne látszódjon az Orders táblában olyan sor, amit a felhasználó a Basket-ből törölt.
                otm.deleteLastRow();
                //Csökkentjük egygel a Basket számlálót, hogy a következő rendelés ID-ja az eddig meglevők után következzen.
                basketCounter--;
                btm.deleteLastRow();
                actualOrders.remove(actualOrders.size() - 1);
            }else{
                JOptionPane.showMessageDialog(null, "A kosár üres, nincs több törölni való.");
            }
        }
        if (e.getSource() == cbExtraSauce) {
            if (cbExtraSauce.isSelected()) {
                isExtraSauceChecked = true;
            } else {
                isExtraSauceChecked = false;
            }
        }
        if (e.getSource() == cbDoubleCheese) {
            if (cbDoubleCheese.isSelected()) {
                isDoubleCheeseChecked = true;
            } else {
                isDoubleCheeseChecked = false;
            }
        }
        if (e.getSource() == rbSize32) {
            PizzaGUI.setPizzaSize(32);
        }
        if (e.getSource() == rbSize45) {
            PizzaGUI.setPizzaSize(45);
        }
        if (e.getSource() == btEndOrder) {
            //Ellenőrizzük, hogy a TextField-ek ki vannak-e töltve, nehogy már valaki nyomogassa az order gombot üres mezőkkel.
            if (!ftfCustomerName.getText().trim().equals("") && !ftfCustomerAddress.getText().trim().equals("") && !ftfCustomerPhoneNumber.getText().trim().equals("")) {
                if(!btm.isEmpty()){
                    taSummary.setText("");
                    taSummary.append("Rendelési adatok: \n " + "Név: " + ftfCustomerName.getText() + "\n Cím: " + ftfCustomerAddress.getText()
                        + "\n Telefon: " + ftfCustomerPhoneNumber.getText() + "\n");
                    taSummary.append(actualOrders.toString());
                    //A pizza árának kiszámitásához létrehozunk egy PizzaDough tipusú objektumot, hogy elkérjük tőle a sütési időt.
                    PizzaDough selectedPizzaDough = PizzaDough.getPizzaDough(cbTypeOfDough.getSelectedItem().toString());
                    taSummary.append("\n\nMegjegyzés: " + taCustomerNote.getText() + "\nVárható szállítási idő: " + PizzaService.calculateArrivingTime(selectedPizzaDough) + " perc\nRendelés időpontja: " + LocalDate.now() + "\nVégösszeg: " + endPrice + " Ft\n\nKöszönjük, hogy a \nVincenzo Pizzériát választotta!");
                    //Hozzáadjuk a rendeléseket az Order Map-hez
                    for (int i = 0; i < actualOrders.size(); i++) {
                        Order.addOrderToMap(actualOrders.get(i).getOrderId(), actualOrders.get(i));
                    }
                    //Mentjük fájlba az Order Map-et
                    Order.saveOrders(Order.orders);
                    //Töröljük a textField-ekből az adatokat, illetve a JList-ből a kiválasztásokat, hogy új rendelés jöhessen, új adatbevitellel, új kiválasztásokkal.
                    //A felhasználónevet, cimet és telefont, bejelentkezett felhasználó esetén nem töröljük.    
                    for (User user : User.getUsers()) {
                        if(user.getName().equals(ftfCustomerName.getText().trim())){
                            isLoggedUser = true;
                        }
                    }
                    if(!isLoggedUser){
                        ftfCustomerName.setText("");
                        ftfCustomerAddress.setText("");
                        ftfCustomerPhoneNumber.setText("");
                    }
                    cbDoubleCheese.setSelected(false);
                    cbExtraSauce.setSelected(false);
                    cbQuantity.setSelectedIndex(0);
                    rbSize32.setSelected(true);
                    taCustomerNote.setText("");
                    //selectedToppings.clear();
                    listTopping.clearSelection();
                    //Töröljük a kosár tartalmát
                    btm.clearBasket();
                    //A kosár méret számlálót nullázzuk ki.
                    basketCounter = 0;
                }else{
                    JOptionPane.showMessageDialog(null, "Kérem rakjon legalább egy pizzát a kosárba.!");
                }
            } else {
                JOptionPane.showMessageDialog(null, "Kérem töltse ki a név, cim, telefon mezőket!");
            }
        }
        if (e.getSource() == cbTypeOfBase) {
            layPN.removeAll();
            lbl[8] = new JLabel();
            lbl[8].setIcon(new ImageIcon(imgCheese));
            lbl[8].setBounds(30, 60, 275, 275);
            layPN.add(lbl[8], new Integer[8]);
            if (selectedNameIndinces != null) {
                for (int i = 0; i < selectedNameIndinces.length; i++) {
                    lbl[i] = new JLabel();
                    lbl[i].setIcon(new ImageIcon(imgToppings[selectedNameIndinces[i]]));
                    lbl[i].setBounds(45, 60, 225, 225);
                    layPN.add(lbl[i], new Integer[i]);
                }
            }
            lbl[9] = new JLabel();
            lbl[9].setIcon(new ImageIcon(imgBases[cbTypeOfBase.getSelectedIndex()]));
            lbl[9].setBounds(0, 15, 325, 325);
            layPN.add(lbl[9], new Integer[9]);
        }

        if (e.getSource() == btRegistration) {
            if (!ftfRegName.getText().equals("") && !ftfRegAddress.getText().equals("") && !ftfRegPhoneNumber.getText().equals("") && !(pfRegPassword.getPassword().length == 0)) {
                List<User> users = new ArrayList<>(User.getUsers());
                String userName = ftfRegName.getText().trim();
                int i = 0;
                boolean existingUser = true;
                while (existingUser == true && i < users.size()) {
                    if (userName.equals(users.get(i).getName())) {
                        JOptionPane.showMessageDialog(null, "Már van ilyen nevű felhasználó!");
                        break;
                    }
                    if (i == users.size() - 1) {
                        existingUser = false;
                    }
                    i++;
                }
                if (!existingUser) {
                    if (!Arrays.equals(pfRegPassword.getPassword(), pfRegPasswordCheck.getPassword())) {
                        JOptionPane.showMessageDialog(null, "A megadott jelszavak nem egyeznek");
                    } else {
                        User newUser = new User(ftfRegName.getText(), String.valueOf(pfRegPassword.getPassword()), ftfRegAddress.getText(), ftfRegPhoneNumber.getText(), false);
                        User.addUserToMap(ftfRegName.getText(), String.valueOf(pfRegPassword.getPassword()), ftfRegAddress.getText(), ftfRegPhoneNumber.getText(), false);
                        User.saveUsers(User.users);
                        utm.refreshRows();
                        JOptionPane.showMessageDialog(null, "SIKERES REGISZTRÁCIÓ! BEJELENTKEZHET!");
                        ftfRegName.setText("");
                        ftfRegAddress.setText("");
                        ftfRegPhoneNumber.setText("");
                        pfRegPassword.setText("");
                        pfRegPasswordCheck.setText("");
                        cardLayout.show(mainPanel, "loginCard");
                    }
                }
            } else {
                JOptionPane.showMessageDialog(null, "Kérem töltsön ki minden mezőt!");
            }
        }
        if (e.getSource() == btBackToLogin) {
            cardLayout.show(mainPanel, "loginCard");
        }
        if (e.getSource() == btModifyDough) {
            //Ha az ár mező nem üres és csak számokat tartalmaz
            if ((!tfDoughPrice.getText().trim().isEmpty()) && (tfDoughPrice.getText().matches("[0-9]+"))) {
                List<String> keys = new ArrayList<>(PizzaDough.doughs.keySet());
                String key = keys.get(rowOfDoughsTable);
                //Átirjuk az árat az objektumban.
                PizzaDough.doughs.get(key).setPrice(Double.valueOf(tfDoughPrice.getText()));//NullPointerException elkerülése, mert nem numeric adattal, hanem key-el azonositod a Map elemét
                //Megjelenitjük az új értéket a táblázatban
                doughtm.updateRow(PizzaDough.doughs.get(key));
                //A doughs Map-et kiirjuk fájlba.
                PizzaMaterial.savePizzaMaterials(PizzaDough.doughs, "doughs");
            } else {
                JOptionPane.showMessageDialog(null, "Az ár mezőbe csak számot lehet megadni.");
            }
        }
        if (e.getSource() == btModifyBase) {
            //Ha az ár mező nem üres és csak számokat tartalmaz
            if ((!tfBasePrice.getText().trim().isEmpty()) && (tfBasePrice.getText().matches("[0-9]+"))) {
                //Átirjuk a megjelenitett JTable sorában az árat.
                List<String> keys = new ArrayList<>(PizzaBase.bases.keySet());
                String key = keys.get(rowOfBasesTable);
                //Átirjuk az árat az objektumban.
                PizzaBase.bases.get(key).setPrice(Double.valueOf(tfBasePrice.getText()));//NullPointerException nem numeric adattal, hanem key-el azonositod a Map elemét
                //Megjelenitjük az új értéket a táblázatban
                basetm.updateRow(PizzaBase.bases.get(key));
                //A bases Map-et kiirjuk fájlba.
                PizzaMaterial.savePizzaMaterials(PizzaBase.bases, "bases");
            } else {
                JOptionPane.showMessageDialog(null, "Az ár mezőbe csak számot lehet megadni.");
            }
        }
        if (e.getSource() == btModifyTopping) {
            //Ha az ár mező nem üres és csak számokat tartalmaz
            if ((!tfToppingPrice.getText().trim().isEmpty()) && (tfToppingPrice.getText().matches("[0-9]+"))) {
                List<String> keys = new ArrayList<>(PizzaTopping.toppings.keySet());
                String key = keys.get(rowOfToppingsTable);
                //Átirjuk az árat az objektumban.
                PizzaTopping.toppings.get(key).setPrice(Double.valueOf(tfToppingPrice.getText()));//NullPointerException nem numeric adattal, hanem key-el azonositod a Map elemét
                //Megjelenitjük az új értéket a táblázatban
                toppingtm.updateRow(PizzaTopping.toppings.get(key));
                //A toppings Map-et kiirjuk fájlba.
                PizzaMaterial.savePizzaMaterials(PizzaTopping.toppings, "toppings");
            } else {
                JOptionPane.showMessageDialog(null, "Az ár mezőbe csak számot lehet megadni.");
            }
        }
        if (e.getSource() == btModifyUser) {
            List<String> userNames = new ArrayList<>(User.users.keySet());//---itt is lehet IOOBE
            String userName = userNames.get(rowOfUsersTable);
            //Létrehozunk egy új User-t a módositott adatokkal
            User modifiedUser = new User(userName, tfUserPassword.getText().trim(), tfUserAddress.getText().trim(),tfUserPhone.getText().trim(),chckBoxUserIsAdmin.isSelected());
            //A users MAP-ben a módositott User-rel kicseréljük a régi User-t.
            User.users.replace(userName, modifiedUser);
            //A User Map-et kiirjuk fájlba.
            User.saveUsers(User.users);
            //A User táblamodelt is frissitjük, hogy látszódjon a módositott érték a sorban.
            utm.refreshRows();
            
        }
    }

    //listázza az aktuális rendelést az Order osztály toString-jével
    public String listActualOrders() {
        return actualOrders.toString();
    }
    public static int getPizzaSize() {
        return pizzaSize;
    }
    public static void setPizzaSize(int pizzaSize) {
        PizzaGUI.pizzaSize = pizzaSize;
    }
    public boolean isIsDoubleCheeseChecked() {
        return isDoubleCheeseChecked;
    }
    public void setIsDoubleCheeseChecked(boolean isDoubleCheeseChecked) {
        this.isDoubleCheeseChecked = isDoubleCheeseChecked;
    }
    public boolean isIsExtraSauceChecked() {
        return isExtraSauceChecked;
    }
    public void setIsExtraSauceChecked(boolean isExtraSauceChecked) {
        this.isExtraSauceChecked = isExtraSauceChecked;
    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getSource() == listTopping) {
            selectedNameIndinces = listTopping.getSelectedIndices();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getSource() == listTopping) {
            layPN.removeAll();
            lbl[8] = new JLabel();
            lbl[8].setIcon(new ImageIcon(imgCheese));
            lbl[8].setBounds(30, 60, 275, 275);
            layPN.add(lbl[8], new Integer[8]);
            for (int i = 0; i < selectedNameIndinces.length; i++) {
                lbl[i] = new JLabel();
                lbl[i].setIcon(new ImageIcon(imgToppings[selectedNameIndinces[i]]));
                lbl[i].setBounds(45, 60, 225, 225);
                layPN.add(lbl[i], new Integer[i]);
            }
            lbl[9] = new JLabel();
            lbl[9].setIcon(new ImageIcon(imgBases[cbTypeOfBase.getSelectedIndex()]));
            lbl[9].setBounds(0, 15, 325, 325);
            layPN.add(lbl[9], new Integer[9]);
        }
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
