package tablemodel;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.PizzaMaterial;
import model.PizzaTopping;

public class ToppingTableModel extends AbstractTableModel{
    private List<PizzaTopping> toppings;
    private static final List<String> COLUMN_NAMES = new ArrayList<>();
    private static final List<Class<?>> COLUMN_CLASSES = new ArrayList<>();
    
    static{
        COLUMN_NAMES.add("Name");
        COLUMN_NAMES.add("Price");
        
        COLUMN_CLASSES.add(String.class);
        COLUMN_CLASSES.add(String.class);
    }

    public ToppingTableModel(List<PizzaTopping> topping) {
        this.toppings = topping;
        
    }
    
    public void addRow(PizzaTopping topping){
        toppings.add(topping);
        int row = toppings.size()-1;
        fireTableRowsInserted(row,row);
    }
    public void deleteRow(PizzaTopping topping){
        int row = toppings.indexOf(topping);
        toppings.remove(topping); 
        fireTableRowsDeleted(row, row);
    }
    public void updateRow(PizzaTopping topping){
        int row = toppings.indexOf(topping);
        fireTableRowsUpdated(row, row);
    }
    public void modifyPrice(PizzaTopping topping, Double newPrice){
        int row = toppings.indexOf(topping);
        toppings.get(row).setPrice(newPrice);
        fireTableRowsDeleted(row, row);
    }

    @Override
    public String getColumnName(int column) {
        return COLUMN_NAMES.get(column);
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return COLUMN_CLASSES.get(columnIndex);
    }
    
    
    @Override
    public int getRowCount() {
        return toppings.size();
    }

    @Override
    public int getColumnCount() {
        return PizzaMaterial.class.getDeclaredFields().length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case(0) : return toppings.get(rowIndex).getName();
            case(1) : return toppings.get(rowIndex).getPrice();
            default: return null;
        }
    }
    
}

