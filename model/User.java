package model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class User implements Serializable, Comparable<User>{
    
    private String name;
    private String address;
    private String phone;
    private String password;
    private boolean isAdmin;
    
    public static Map<String,User> users = new HashMap<>();

    public User(String name, String password, String address, String phone, boolean isAdmin) {
        this.name = name;
        this.password = password;
        this.address = address;
        this.phone = phone;
        this.isAdmin = isAdmin;
    }
    
    public static void addUserToMap(String name, String password, String address, String phone, boolean isAdmin){
        User u = User.users.get(name);
        //Ha nincs akkor létrehozunk egy újat és beletesszük a Map-be.
        if(u==null){
            u=new User(name, password, address, phone, isAdmin);
            User.users.put(name, u);
        }
    }
    public static void addModifyUserToMap(String name, User user){
            User.users.put(name, user);
    }
    
    public static void removeUser(User user){
        users.remove(user.getName(), user);
    }
    
    public static User getUser(String userName){
        User u = User.users.get(userName);
        return u;
    }
    
    public static Collection<User> getUsers(){
	return users.values();
    }
    
    @Override
    public int compareTo(User u) {
        return getName().compareTo(u.getName());
    }
    
    //Szerializálás, objektumok kiirása
    //A Map-et irjuk ezzel ki.
    public static void saveUsers(Map<String, User> map){
        File f = new File("src/db/users.dat");//Lérehozunk egy fájl objektumot, ami vár egy Stringet, hogy a fájl hol helyezkedik el.
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(f);//Létrehozunk egy kiiró Stream-et.
            ObjectOutputStream oos = new ObjectOutputStream(fos); //Létrehozunk egy objektum kiirót,
            oos.writeObject(map); //és átadjuk neki a kiirandó Map-et.
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(fos!=null){
                try {
                        fos.close(); //A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
	
    //Deszerializálás, objektumok visszaolvasása
    //Map-et olvasunk be.
    public static void loadUsers(Map<String, User> map){
        FileInputStream fis = null;
        try {
            fis = new FileInputStream("src/db/users.dat");//Látrehozunk egy olyan Stream-et, amiből lehet olvasni.
            ObjectInputStream ois = new ObjectInputStream(fis);//Létrehozunk egy objektum olvasót,
            map = (Map<String, User>)ois.readObject();//és a Map-be tesszük a kiolvasott objektumot, Map-re kasztolva.
            User.users = (Map<String, User>) map;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally{
            if(fis!=null){
                try {
                        fis.close();//A fájlt mindenképp le kell zárni.
                } catch (IOException e) {
                        e.printStackTrace();
                }
            }
        }
    }
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public String toString() {
        return "User{" + "name=" + getName() + ", password="+getPassword()+", address=" + getAddress() + ", phone=" + getPhone() + ", Admin-e="+isAdmin+'}';
    }

}
