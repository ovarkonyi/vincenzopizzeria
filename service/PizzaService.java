package service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map;
import model.Order;
import model.PizzaBase;
import model.PizzaDough;
import model.PizzaMaterial;
import model.PizzaTopping;
import view.PizzaGUI;

/*
 * Ebben az osztályban vannak azok a metódusok, 
 * amelyek a GUI-n a kiválasztásokra vagy gombnyomásokra lefutnak,
 * illetve amelyek a program logikáját képezik.
 */
public class PizzaService {

    public static final int AVG_TIME_OF_TRANSFERING = 30;
    public static final int GIFT_LIMIT = 3000;
    public static final double PRICE_OF_CHEESE = 100;
    public static final double PIZZA_PRICE_THREE_TOPPINGS = 1400.0;
    public static final double BASE_PIZZA_PRICE = 800.0;
    public static final int DISCOUNT_LIMIT = 6;
    

    //Megjelenitjük a GUI-t.
    public void makeGUI() {
        new PizzaGUI();

    }

    //Kiszámoljuk egy pizza árát.
    public double calculateOnePizzaPrice(ArrayList<PizzaMaterial> listOfPizzaMaterials, int pieces, boolean extraSauce, boolean doubleCheese) {
        double onePizzaPrice = 0.0;
        if (listOfPizzaMaterials.size() < DISCOUNT_LIMIT) {
            onePizzaPrice = PIZZA_PRICE_THREE_TOPPINGS;
        } else {
            onePizzaPrice = BASE_PIZZA_PRICE;
            for (int i = 0; i < listOfPizzaMaterials.size(); i++) {
                onePizzaPrice += listOfPizzaMaterials.get(i).getPrice();
            }
        }
        if (extraSauce) {
            onePizzaPrice += listOfPizzaMaterials.get(1).getPrice();
        }
        if (doubleCheese) {
            onePizzaPrice += PRICE_OF_CHEESE;
        }
        if (PizzaGUI.getPizzaSize() == 45) {
            onePizzaPrice *= 1.3;
        }
        System.out.println("A rendelt pizza ára: " + onePizzaPrice * pieces);
        return onePizzaPrice * pieces;
    }

    //Kiszámoljuk a pizza érkezési idejét: A pizza tésztától függőan a sütési idejét összeadjuk a kiszállitási idővel.
    public static double calculateArrivingTime(PizzaDough dough) {
        return dough.getCookingTime(dough) + AVG_TIME_OF_TRANSFERING;
    }

    //Eldöntjük, hogy a rendelés értéke nagyobb mint GIFT_LIMIT.
    public boolean isOrderBiggerThanGiftLimit(double price) {
        if (price > GIFT_LIMIT) {
            return true;
        }
        return false;
    }

    //Ha a rendelés nagyobb mint GIFT_LIMIT, akkor ajándékot adunk.
    public void giveGift(boolean isBigger) {
        if (isBigger) {
            System.out.println("Ügyes voltál kapsz kólát!");
        }
    }
}
