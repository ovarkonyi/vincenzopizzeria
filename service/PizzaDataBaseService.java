 package service;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.Map;
import model.Order;
import model.PizzaBase;
import model.PizzaDough;
import model.PizzaMaterial;
import model.PizzaTopping;
import model.User;

public class PizzaDataBaseService {
    
    //A tészták MAP-et feltöltjük kezdeti értékekkel --- működik
    public void pizzaDoughsInit(){
        PizzaDough.addPizzaDoughToMap("vékony tészta", 250); 
        PizzaDough.addPizzaDoughToMap("normál tészta", 250);
        PizzaDough.addPizzaDoughToMap("vastag tészta", 250);
    }
    //Az alapok MAP-et feltöltjük kezdeti értékekkel --- működik
    public void pizzaBasesInit(){
        PizzaBase.addPizzaBaseToMap("paradicsomos", 200); 
        PizzaBase.addPizzaBaseToMap("tejfölös", 200);
        PizzaBase.addPizzaBaseToMap("fokhagymás", 200);
        PizzaBase.addPizzaBaseToMap("BBQ-s", 200);
    }
    //A feltétek MAP-et feltöltjük kezdeti értékekkel --- működik
    public void pizzaToppingInit(){
        PizzaTopping.addPizzaToppingToMap("sonka", 115); 
        PizzaTopping.addPizzaToppingToMap("gomba", 125);
        PizzaTopping.addPizzaToppingToMap("szalámi", 135);
        PizzaTopping.addPizzaToppingToMap("hagyma", 50);
        PizzaTopping.addPizzaToppingToMap("kolbász", 150);
        PizzaTopping.addPizzaToppingToMap("pepperoni", 150);
        PizzaTopping.addPizzaToppingToMap("paradicsom", 50);
        PizzaTopping.addPizzaToppingToMap("rukkola", 80);
    }
    //A felhasználók MAP-et feltöltjük kezdeti értékekkel --- működik
    public void usersInit(){
        User.addUserToMap("Orsi", "1234", "Budapest 1035 Kecskevirág utca 5.", "06 30 123 4566", false); 
        User.addUserToMap("Ádám", "1234", "Kecskemét 3412 Búza utca 10.", "06 20 456 6778", false);
        User.addUserToMap("Béla", "1234", "Szolnok 4619 Kossuth utca 10.", "06 70 273 9278", false);
        User.addUserToMap("Zoli", "1234", "Hajdúnánás 6482 Fő utca 10.", "06 70 244 1378", false);
        User.addUserToMap("Admin", "1234", "Budapest 1087 Nagyfuvaros utca 14.", "06 20 554 9125", true);
        User.addUserToMap("Teszt", "", "Budapest 1087 Nagyfuvaros utca 14.", "06 20 554 9125", false);
    }
    
     //A felhasználók MAP-et feltöltjük kezdeti értékekkel --- működik
    public void ordersInit(){
//        Order.addOrderToMap(1000, "Hamar kérem", 1, "2019-07-15", 1600.0, "Zoli", "vastag tészta", "paradicsomos", "hagyma, rukkola"); 
//        User.addUserToMap("Ádám", "1234", "Kecskemét 3412 Búza utca 10.", "06 20 456 6778", false);
//        User.addUserToMap("Béla", "1234", "Szolnok 4619 Kossuth utca 10.", "06 70 273 9278", false);
//        User.addUserToMap("Zoli", "1234", "Hajdúnánás 6482 Fő utca 10.", "06 70 244 1378", false);
//        User.addUserToMap("Admin", "1234", "Budapest 1087 Nagyfuvaros utca 14.", "06 20 554 9125", true);
//        User.addUserToMap("Teszt", "", "Budapest 1087 Nagyfuvaros utca 14.", "06 20 554 9125", false);
    }
    
    //Feltöltjük a MAP-eket objektumokkal. Ha még üresek, akkor kezdő értékekkel látjuk el --- működik
    public void initPizzaDataBase(){
        try{
            File doughsFile = new File("src/db/doughs.dat");
            File basesFile = new File("src/db/bases.dat");
            File toppingsFile = new File("src/db/toppings.dat");
            File usersFile = new File("src/db/users.dat");
            File ordersFile = new File("src/db/orders.dat");
            //Ha a fájlok léteznek és nem mappák, akkor töltse be azokat.
            if((doughsFile.exists() && !doughsFile.isDirectory()) && (basesFile.exists() && !basesFile.isDirectory()) && (toppingsFile.exists() && !toppingsFile.isDirectory()) && (usersFile.exists() && !usersFile.isDirectory()) && (ordersFile.exists() && !ordersFile.isDirectory())) { 
                loadPizzaMaterials();
                loadUsers();
                loadOrders();
            //Egyébként meg töltse fel kezdő értékekkel a MAP-eket.
            }else{
                System.out.println("A pizza alapanyagok vagy a felhasználók vagy a rendelések nem tölthetőek be a MAP-ekbe, a fájl nem található! A MAP-eknek kezdő értéket adunk.");
                initPizzaMaterials();
                initUsers();
            }
        }catch(Exception e){
            System.out.println("Az adatbázist reprezentáló .dat fájlok nem találhatóak a a program mappában.");
        }
    }
    

    //A pizza alapanyag MAP-eket kezdő értékekkel látjuk el. --- működik
    public void initPizzaMaterials(){
        pizzaDoughsInit();
        pizzaBasesInit();
        pizzaToppingInit();
    }
    //A pizza alapanyag MAP-eket elmentjük fájlokba. Serializálunk. --- működik
    public void savePizzaMaterials(){
        PizzaMaterial.savePizzaMaterials(PizzaDough.doughs, "doughs");
        PizzaMaterial.savePizzaMaterials(PizzaBase.bases, "bases");
        PizzaMaterial.savePizzaMaterials(PizzaTopping.toppings, "toppings");
    }
    //A pizza alapanyag MAP-eket feltöltjük fájlokból. Deserializálunk. ---  működik
    public void loadPizzaMaterials(){
        PizzaMaterial.loadPizzaMaterials(PizzaDough.doughs, "doughs");
        PizzaMaterial.loadPizzaMaterials(PizzaBase.bases, "bases");
        PizzaMaterial.loadPizzaMaterials(PizzaTopping.toppings, "toppings");
    }
   
    //A users MAP-et kezdő értékkel látjuk el. --- működik
    public void initUsers(){
        usersInit();
    }
    //A users MAP-et elmentjük fájlba. Serializálunk. --- működik
    public void saveUsers(){
        User.saveUsers(User.users);
    }
    //A users MAP-et feltöltjük fájlből. Deserializálunk. --- működik
    public void loadUsers(){
        User.loadUsers(User.users);
    }

    //Az Orders MAP-et kezdő értékkel látjuk el. --- működik
    public void initOrders(){
        ordersInit();
    }
    //Az Orders MAP-et elmentjük fájlba. Serializálunk. --- működik
    public void saveOrders(){
        Order.saveOrders(Order.orders);
    }
    //Az Orders MAP-et feltöltjük fájlből. Deserializálunk. --- működik
    public void loadOrders(){
        Order.loadOrders(Order.orders);
    }
}
